package com.beta;

import io.quarkus.runtime.annotations.QuarkusMain;
import io.quarkus.runtime.QuarkusApplication;
import org.jboss.logging.Logger;

@QuarkusMain
public class HelloWorldCmd implements  QuarkusApplication{
    private static final Logger LOG = Logger.getLogger(HelloWorldCmd.class);

    @Override
    public int run(String... args) throws Exception {
        int count=60;
        int exitCode = 0;

        if(args.length>0){
            count = Integer.parseInt(args[0]);
        }

        LOG.info("count = " + count);

        if(args.length>1){
            exitCode = Integer.parseInt(args[1]);
        }

        LOG.info("exitCode = " + exitCode);

        while(count>0){
            LOG.info("Hello World " + count);
            LOG.error("Fake Error World " + count);
            System.err.println("Error World " + count);
            count--;
            Thread.sleep(1000);
        }

        return exitCode;
    }
}
